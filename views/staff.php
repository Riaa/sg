<?php
ob_start();
use Mpdf\Utils\UtfString;
include('menu.php');
include('header.php');
include('session.php');
include_once('printscript.php');

$objController = new App\Controller\Controller();

$objController->setData($_SESSION);
if(isset($_GET) || isset($_GET['email'])){
	$_GET['singleView']='Yes';
	$objController->setData($_GET);
}
?>
<?php
if($_SESSION['loginas']=='Admin'){
	$allmembers=$objController->view($_GET);
  //var_dump($allmembers);
}
//var_dump($_GET);
//var_dump($_SESSION); die();

?>
    <!-- Page Content -->
    <div class="container">
    	<div class="">
<style>
	#membersview th,td{
		text-align: center;
	}
</style>
	<a href="admin.php?staff=addstaff" class="btn btn-primary" role="button" >ADD NEW STAFF</a>


		<table id="membersview" border="1" width="100%" style="border-collapse: collapse; ">
			<thead>
			<tr>
				<th>ID</th><th>Name</th><th>Email</th><th>Mobile</th><th>Actions</th>
			</tr>
			</thead>

			<?php

			foreach($allmembers as $singleMember){

				echo "<tr><td>$singleMember->user_id</td><td>$singleMember->last_name</td><td>$singleMember->email</td><td>$singleMember->phone</td>


				<td>
				<a class='btn btn-primary' href='admin.php?email=$singleMember->email&admin=staff'>View</a>
				<a class='btn btn-warning' href='admin.php?email=$singleMember->email&edit=editstaff&admin=staff&id=$singleMember->user_id'>Edit</a>

				 <a class='btn btn-danger' href='delete.php?email=$singleMember->email&deleteid=$singleMember->user_id'>Delete</a>
				</td>
			</tr>
				";
			}


			?>


		</table>

		</div>
	</div>	
    
<?php


include('footer.php');
include('footer_script.php');
?>