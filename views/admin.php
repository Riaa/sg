<?php
ob_start();
/*
$html = <<<PROFILE



 <div   class="container ">
          <div align="center" class="row cardcenter">
              <div id="divprintview" style="width:595px; border:1px solid #CCCCCC;border-radius:5px;padding-left:30px; margin-bottom:10px;" class="divprintview profile col-md-6 col-md-offset-3">
                  <div class="text-center up cardcenter">
                      <h3 class="titA cardcenter">Skygoal Synergy</strong><br/></h3>
                      <p class="cardcenter">
                      VISA PROCESSING CONSULTING FIRM IN INDIA, POLAND & UAE
                     </p>
                    </div>
                  <div align="center"  class="row table-responsive">
                      <table  class="table">
                          <thead>
                          <tr>
                              <td class="cardcenter" style="text-align: center;" colspan="3"><u><b></b></u></td>
                          </tr>
                          <tr>
                              <td class="cardcenter" style="text-align: center;" colspan="3"><b>Admin's Information</b><br>

                              </td>
                          </tr>

                          </thead>
                          <tr>
                              <td>Name </td> <td>:</td><td >$last_name</td>
                          </tr>
                          <tr>
                            <td>Email</td> <td>:</td><td>$email</td>
                          </tr>
                          <tr>
                            <td>Phone</td>
                            <td>:</td>
                            <td>$phone</td>
                          </tr>
                          <tr>
                            <td>Address</td> <td>:</td><td>$address</td>
                          </tr>

                      </table>

                  </div>

              </div>
          </div>
        </div>
PROFILE;
*/
include('menu.php');
include('header.php');
include('session.php');
include_once('printscript.php');

$objController = new App\Controller\Controller();
$objController->setData($_SESSION);

################################### add staff ##############################


if(isset($_GET['staff'])){

//	echo $addstaff;
}

################################### add staff ##############################

if(isset($_GET) || isset($_GET['email'])) {
	$_GET['singleView'] = 'Yes';
	$objController->setData($_GET);
	$objToArrayProfile = $objController->objectToArray($objController->view($_GET));
		//var_dump($_GET);
	$last_name = $objToArrayProfile['0']['last_name'];
	$email = $objToArrayProfile['0']['email'];
	$password = $objToArrayProfile['0']['password'];
	$address = $objToArrayProfile['0']['address'];
	$phone = $objToArrayProfile['0']['phone'];
	$viewType="";
	$submitType="";
	$submitButton='';
	$backbutton='<a href="staff.php?admin=allstaff" class="btn btn-primary" role="button">Go Back</a>';
	$adminPassword="<div class=\"form-group\">
							<label class=\"col-md-4 control-label\">Password</label>
							<div class=\"col-md-8 inputGroupContainer\">
								<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-lock\"></i></span><input id=\"password\" name=\"password\" placeholder=\"password\" class=\"form-control\" required  $viewType type=\"password\"></div>
							</div>
						</div>";

	if(isset($_GET['email']) && $_GET['admin']=='staff'){
		$viewType='Readonly';
		$backbutton;
		if($_GET['admin']=='staff' && $_GET['edit']=='editstaff'){
			$viewType=' ';
			$submitButton="<button name=\"submit\"  type=\"submit\" class=\"btn btn-primary\">Submit</button>";
			$submitType="<input type=\"hidden\" name=\"edit\" value=\"editstaff\">";
			$hiddenId="<input type=\"hidden\" name=\"id\" value=".$_GET['id'].">";
			$backbutton;
		}else{
			$adminPassword=" ";
		}

	}

	//
	else if(isset($_GET['staff']) && $_GET['staff']=='addstaff'){
		$viewType=' ';
		$last_name ='';
		$email ='';
		$address ='';
		$phone ='';
		$submitButton="<button name=\"submit\"  type=\"submit\" class=\"btn btn-primary\">Submit</button>";
		$submitType="<input type=\"hidden\" name=\"add\" value=\"addstaff\">";
		$backbutton;

	}
	if(isset($_GET['admin']) && $_GET['admin']=='admin' ){
		$viewType='Readonly';
	}

	$addstaff=<<<ADDSTAFF
<div class="container">
	<div class="row">
	<form class="" method="POST" action="store.php" enctype="multipart/form-data">
	$submitType
	$hiddenId
		<div class="col-md-3"></div>

			<div class="col-md-6">
				<div class="well form-horizontal">
					<fieldset>
						<div class="form-group">
							<label class="col-md-4 control-label">Full Name</label>
							<div class="col-md-8 inputGroupContainer">
								<div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="fullName" name="fullName" placeholder="Full Name" class="form-control" required="true" value="$last_name" $viewType type="text"></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Address </label>
							<div class="col-md-8 inputGroupContainer">
								<div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="addressLine1" name="addressLine1" placeholder="Address Line 1" class="form-control" required="true" value="$address" $viewType type="text"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-8 inputGroupContainer">
								<div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span><input id="email" name="email" placeholder="Email" class="form-control" required value="$email" $viewType type="email"></div>
							</div>
						</div>
						$adminPassword
						<div class="form-group">
							<label class="col-md-4 control-label">Phone Number</label>
							<div class="col-md-8 inputGroupContainer">
								<div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="phoneNumber" name="phoneNumber" placeholder="Phone Number" class="form-control" required="true" value="$phone" $viewType type="text"></div>
							</div>
						</div>
						<div class="text-right">$submitButton</div>
					</fieldset>

				</div>

				 <div class="text-center">$backbutton</div>
			</div>

		<div class="col-md-3"></div>
</form>
	</div>
</div>
ADDSTAFF;

		?>
		<div class="container">
			<section id="inner-headline">
				<div class="container">
					<div class="row">
						<div class=" col-md-4">
							<div class="inner-heading">

							</div>
						</div>
						<div class=" col-md-8">
							<ul style="background-color:inherit;" class="breadcrumb">
								<li><a href="home.php"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>

							</ul>
						</div>
					</div>
				</div>
			</section>
			<div id="dvContainer">
				<style>
					<?php
                            include ('../resource/css/printsetup.css');
                    ?>
				</style>
				<section id="content">

					<?php
					echo $addstaff;
					?>

				</section>
			</div>


		</div>
		<?php


}

include('footer.php');
include('footer_script.php');

?>

