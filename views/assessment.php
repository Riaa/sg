<?php
ob_start();
use Mpdf\Utils\UtfString;
include('menu.php');
include('header.php');
include('session.php');
include_once('printscript.php');

$objController = new App\Controller\Controller();

$objController->setData($_SESSION);
if(isset($_GET) || isset($_GET['email'])){
  $_GET['singleView']='Yes';
  $objController->setData($_GET);
}

$objToArrayProfile=$objController->objectToArray($objController->view($_GET));

//var_dump($objToArrayProfile);

$id=$objToArrayProfile['0']['id'];
$password=$objToArrayProfile['0']['password'];
$fullName=$objToArrayProfile['0']['fullName'];
$fatherName=$objToArrayProfile['0']['fatherName'];
$motherName=$objToArrayProfile['0']['motherName'];
$addressLine1=$objToArrayProfile['0']['addressLine1'];
$addressLine2=$objToArrayProfile['0']['addressLine2'];
$city=$objToArrayProfile['0']['city'];
$state=$objToArrayProfile['0']['state'];
$postcode=$objToArrayProfile['0']['postcode'];
$country=$objToArrayProfile['0']['country'];
$email=$objToArrayProfile['0']['email'];
$phoneNumber=$objToArrayProfile['0']['phoneNumber'];
$dob=$objToArrayProfile['0']['dob'];
$gender=$objToArrayProfile['0']['gender'];
$passport=$objToArrayProfile['0']['passport'];
$birthcert=$objToArrayProfile['0']['birthcert'];
$passport_file=$objToArrayProfile['0']['passport_file'];
$picture=$objToArrayProfile['0']['picture'];
$brc=$objToArrayProfile['0']['brc'];
$cv=$objToArrayProfile['0']['cv'];
$comment=$objToArrayProfile['0']['comment'];
$tmpPassword=$_SESSION['0']['tmp_password'];

$assessment=<<<ASSFORM
<form role="form" action="store.php" enctype="multipart/form-data" method="post">
            <input type="hidden" name="assessment" value="assessment">
            <input type="hidden" name="id" value="$id">

              <!--step 1 -->
              <div class="row setup-content" id="step-1">
                <div class="">
                  <div class="col-md-12">
                    <h3> Step 1</h3>
                    <fieldset class="part">
                      <h6 class="subtit">Assesment Sheet:</h6>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label class="control-label">Assesment Sheet For</label>
                            <input  maxlength="100" name="asfor" type="text" class="form-control" placeholder="Assesment Sheet For" required/>
                          </div>
                          <div class="col-md-6">
                            <label class="control-label">File No.</label>
                            <input  maxlength="100" name="fileNo" type="text" class="form-control" placeholder="File No." required/>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label class="control-label">Reference Name</label>
                            <input  maxlength="100" name="refEm" type="text" class="form-control" placeholder="Reference Name By Employer" required/>
                          </div>
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-12">
                                <label class="control-label">Reference Name</label>
                                <select name="refSkygoal" class="form-control" onchange="myRef(this)" required>
                                  <option value="">Select Reference</option>
                                  <option value="Skygoal Synergy">Skygoal Synergy</option>
                                  <option value="Saifur Rahman Chowddhury">Saifur Rahman Chowddhury</option>
                                  <option value="Delwar Hossen">Delwar Hossen</option>
                                  <option value="Surendra Roka">Surendra Roka</option>
                                  <option value="Others">Others</option>
                                </select>
                              </div>
                              <div class="col-md-12">
                                <div id="option" style="display: none;">
                                  <input  maxlength="100" name="refOther" type="text" class="form-control" placeholder="Other refference"/>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Consultant Name</label>
                        <input  maxlength="100" type="text" name="consltName" class="form-control" placeholder="Consultant Name"  required/>
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label class="control-label">Apply Country Name</label>
                            <select name="Applycountry" class="form-control" required>
                              <option value="">Select Country</option>
                              <option value="Poland">Poland</option>
                              <option value="Hungary">Hungary</option>
                              <option value="Czech Republic">Czech Republic</option>
                              <option value="Estonia">Estonia</option>
                            </select>
                          </div>
                          <div class="col-md-6">
                            <label class="control-label">Type of Work</label>
                            <input  maxlength="100" name="toWork" type="text" class="form-control" placeholder="Type of Work" required/>
                          </div>
                        </div>
                      </div>
                    </fieldset>

                    <fieldset class="part">
                      <h6 class="subtit">Passport Data :</h6>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label class="control-label">Surname</label>
                            <input  maxlength="100" name="surName" type="text" value="$fullName" class="form-control" placeholder="Surname" onkeyup="FName()" required/>
                          </div>
                          <div class="col-md-6">
                            <label class="control-label">Given Name</label>
                            <input  maxlength="100" name="givenName" type="text" value="$fullName" class="form-control" placeholder="Given Name" onkeyup="FName()" required/>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Candidate Full Name</label>
                        <input  maxlength="100" name="fullName" id="Full_name" value="$fullName" type="text" class="form-control" placeholder="Full Name" readonly="readonly" />
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label class="control-label">Father`s Name</label>
                            <input  maxlength="100" name="fathersName"  value="$fatherName" type="text" class="form-control" placeholder="Father`s Name" required/>
                          </div>
                          <div class="col-md-6">
                            <label class="control-label">Mother`s Name</label>
                            <input  maxlength="100" name="mothersName" type="text" value="$motherName" class="form-control" placeholder="Mother`s Name" required/>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="marit">
                              <label class="control-label">Marital Status</label>
                              <label class="radio-inline">
                                <input type="radio" name="maritalStatus" value="Yes" onchange="maritStatus(this)"> Yes
                              </label>
                              <label class="radio-inline">
                                <input type="radio" name="maritalStatus" value="No" onchange="maritStatus(this)"> No
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div id="marit_option" style="display: none;">
                              <label class="control-label">Spouse`s Name</label>
                              <input  maxlength="100" name="spouseName" type="text" class="form-control" placeholder="Spouse`s Name"/>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-4">
                              <label class="control-label">Date Of Birth</label>
                              <input  maxlength="100" name="dob"  value="$dob" type="date" class="form-control datepicker" placeholder="Date Of Birth" required/>
                            </div>
                            <div class="col-md-4">
                              <label class="control-label">Age</label>
                              <input  maxlength="100" name="age" type="number" class="form-control" placeholder="Age" required/>
                            </div>
                            <div class="col-md-4">
                              <label class="control-label">Home Postal/Zip code</label>
                              <input  maxlength="100" name="pzcode" type="text" class="form-control" value="$postcode" placeholder="Home Postal/Zip code" required/>
                            </div>

                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-6">
                              <label class="control-label">Place Of Birth</label>
                              <input  maxlength="100" name="pob" type="date" class="form-control" value="$dob" placeholder="Place Of Birth" required/>
                            </div>
                            <div class="col-md-6">
                              <label class="control-label">Candidate`s Citizenship</label>
                              <select name="citizenship" class="form-control" required>
                                <option value="">Select Country</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Nepal">Nepal</option>
                                <option value="Sri Lanka">Sri Lanka</option>
                                <option value="Bhutan">Bhutan</option>
                                <option value="Afganistan">Afganistan</option>
                                <option value="The Maldives">The Maldives</option>
                                <option value="India">India</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-4">
                              <label class="control-label">Passport No</label>
                              <input  maxlength="100" name="passportNo" type="text" value="$passport" class="form-control" placeholder="Running" required/>
                            </div>
                            <div class="col-md-4">
                              <label class="control-label">Old Passport No</label>
                              <input  maxlength="100" name="passportNoOld" type="number"  value="$passport" class="form-control" placeholder="Old" required/>
                            </div>
                            <div class="col-md-4">
                              <label class="control-label">Place of Issue</label>
                              <select name="placeOfissue" class="form-control" required>
                                <option value="">Select place</option>
                                <option value="Dhaka">Dhaka [for Bangladesh]</option>
                                <option value="Mofa">Mofa [for Nepal]</option>
                                <option value="Colombo">Colombo [for Sri Lanka]</option>
                                <option value="Thimpu">Thimpu [for Bhutan]</option>
                                <option value="Kabul">Kabul [for Afganistan]</option>
                                <option value="Male">Male [for The Maldives]</option>
                                <option value="State">State of India [for India]</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-6">
                              <label class="control-label">Passport Issue Date</label>
                              <input  maxlength="100" name="passportIssuedate" type="date" class="form-control datepicker" placeholder="Passport Issue Date" required/>
                            </div>
                            <div class="col-md-6">
                              <label class="control-label">Passport Expiry Date</label>
                              <input  maxlength="100" name="passportExpirydate" type="date" class="form-control datepicker" placeholder="Passport Expiry Date" required/>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">National ID</label>
                          <input  maxlength="100" name="NID" type="number" value="$birthcert" class="form-control" placeholder="National ID" required/>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Permanent Address</label>
                          <div class="row">
                            <div class="col-md-6">
                              <input  maxlength="100" name="permAddress1"  value="$addressLine1" type="text" class="form-control" placeholder="Address line 1 required"/>
                            </div>
                            <div class="col-md-6">
                              <input  maxlength="100" name="permAddress2"  value="$addressLine2" type="text" class="form-control" placeholder="Address line 2"/>
                            </div>
                          </div><br/>
                          <div class="row">
                            <div class="col-md-3">
                              <input type="text" name="post_office" class="form-control" placeholder="Post office" required>
                            </div>
                            <div class="col-md-3">
                              <input type="text" name="zip_code" class="form-control"value="$postcode" placeholder="Post code / Zip code" required>
                            </div>
                            <div class="col-md-3">
                              <input type="text" name="state" class="form-control"  value="$state" placeholder="State/Province/Region" required>
                            </div>
                            <div class="col-md-3">
                              <input type="text" name="country" class="form-control"   value="$country" placeholder="Country" required>
                            </div>
                          </div>

                        </div>

                        <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button>
                    </fieldset>
                  </div>
                </div>
              </div>
                       <div class="text-center"><a href="applicants.php" class="btn btn-primary" role="button">Go Back</a></div>
              <!--step 2 -->
              <div class="row setup-content" id="step-2">
                <div class="">
                  <div class="col-md-12">
                    <h3> Step 2</h3>

                    <fieldset class="part">
                      <h6 class="subtit">Educational Data:</h6>
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                          <tr>
                            <td>Degree</td>
                            <td>Subject/Group</td>
                            <td>Board</td>
                            <td>Passing Year</td>
                            <td>Result</td>
                          </tr>
                          <tr>
                            <td>
                              <div class="form-group">
                                <select name="ssc" class="form-control" required>
                                  <option value="">Select</option>
                                  <option value="S.S.C">S.S.C</option>
                                  </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <select name="sscgroup" class="form-control" required>
                                  <option value="">Select</option>
                                  <option value="Science">Science</option>
                                  <option value="Humanities">Humanities</option>
                                  <option value="Business Studies">Business Studies</option>
                                  <option value="Business Studies">Business Studies</option>
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <select name="sscboard" class="form-control" required>
                                  <option value="">Select</option>
                                  <option value="Dhaka">Dhaka</option>
                                  <option value="Chittagong">Chittagong</option>
                                  <option value="Comilla">Comilla</option>
                                  <option value="Rajshahi">Rajshahi</option>
                                  <option value="Jessore">Jessore</option>
                                  <option value="Barishal">Barishal</option>
                                  <option value="Dinajpur">Dinajpur</option>
                                  <option value="Madrasah">Madrasah</option>
                                  <option value="Madrasah">Madrasah</option>
                                  <option value="Technical">Technical</option>
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <select name="sscpassingYear" class="form-control" required>
                                  <option value="">Select</option><option value="2019">2019</option>
                                  <option value="2018">2018</option><option value="2017">2017</option>
                                  <option value="2016">2016</option><option value="2015">2015</option>
                                  <option value="2014">2014</option><option value="2013">2013</option>
                                  <option value="2012">2012</option><option value="2011">2011</option>
                                  <option value="2010">2010</option><option value="2009">2009</option>
                                  <option value="2008">2008</option><option value="2007">2007</option>
                                  <option value="2006">2006</option><option value="2005">2005</option>
                                  <option value="2004">2004</option><option value="2003">2003</option>
                                  <option value="2002">2002</option><option value="2001">2001</option>
                                  <option value="2000">2000</option><option value="1999">1999</option>
                                  <option value="1998">1998</option><option value="1997">1997</option>
                                  <option value="1996">1996</option><option value="1995">1995</option>
                                  <option value="1994">1994</option><option value="1993">1993</option>
                                  <option value="1992">1992</option><option value="1991">1991</option>
                                  <option value="1999">1990</option>
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <div class="row">
                                  <div class="col-md-12">
                                    <select name="sscresult" class="form-control" onchange="gpaFunctionA(this)">
                                      <option value="">Select</option>
                                      <option value="1st Division">1st Division</option>
                                      <option value="2nd Division">2nd Division</option>
                                      <option value="3rd Division">3rd Division</option>
                                      <option value="GPA (Out of 4)">GPA (Out of 4)</option>
                                      <option value="GPA (Out of 5)">GPA (Out of 5)</option>
                                    </select>
                                  </div>
                                  <div class="col-md-12">
                                    <div id="CGPA_one" style="display: none;">
                                      <input type="text" class="form-control" name="CGPA4" placeholder="Enter CGPA out of 4">
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div id="CGPA_two" style="display: none;">
                                      <input type="text" class="form-control" name="CGPA5" placeholder="Enter CGPA out of 5">
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div class="form-group">
                                <select name="hsc" class="form-control">
                                  <option value="">Select</option>

                                  <option value="H.S.C">H.S.C</option>

                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <select name="hscgroup" class="form-control">
                                  <option value="">Select</option>
                                  <option value="Science">Science</option>
                                  <option value="Humanities">Humanities</option>
                                  <option value="Business Studies">Business Studies</option>
                                  <option value="Business Studies">Business Studies</option>
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <select name="hscboard" class="form-control">
                                  <option value="">Select</option>
                                  <option value="Dhaka">Dhaka</option>
                                  <option value="Chittagong">Chittagong</option>
                                  <option value="Comilla">Comilla</option>
                                  <option value="Rajshahi">Rajshahi</option>
                                  <option value="Jessore">Jessore</option>
                                  <option value="Barishal">Barishal</option>
                                  <option value="Dinajpur">Dinajpur</option>
                                  <option value="Madrasah">Madrasah</option>
                                  <option value="Madrasah">Madrasah</option>
                                  <option value="Technical">Technical</option>
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <select name="hscpassingYear" class="form-control">
                                  <option value="">Select</option><option value="2019">2019</option>
                                  <option value="2018">2018</option><option value="2017">2017</option>
                                  <option value="2016">2016</option><option value="2015">2015</option>
                                  <option value="2014">2014</option><option value="2013">2013</option>
                                  <option value="2012">2012</option><option value="2011">2011</option>
                                  <option value="2010">2010</option><option value="2009">2009</option>
                                  <option value="2008">2008</option><option value="2007">2007</option>
                                  <option value="2006">2006</option><option value="2005">2005</option>
                                  <option value="2004">2004</option><option value="2003">2003</option>
                                  <option value="2002">2002</option><option value="2001">2001</option>
                                  <option value="2000">2000</option><option value="1999">1999</option>
                                  <option value="1998">1998</option><option value="1997">1997</option>
                                  <option value="1996">1996</option><option value="1995">1995</option>
                                  <option value="1994">1994</option><option value="1993">1993</option>
                                  <option value="1992">1992</option><option value="1991">1991</option>
                                  <option value="1999">1990</option>
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <div class="row">
                                  <div class="col-md-12">
                                    <select name="hscresult" class="form-control" onchange="gpaFunctionB(this)">
                                      <option value="">Select</option>
                                      <option value="1st Division">1st Division</option>
                                      <option value="2nd Division">2nd Division</option>
                                      <option value="3rd Division">3rd Division</option>
                                      <option value="GPA (Out of 4)">GPA (Out of 4)</option>
                                      <option value="GPA (Out of 5)">GPA (Out of 5)</option>
                                    </select>
                                  </div>
                                  <div class="col-md-12">
                                    <div id="CGPA_three" style="display: none;">
                                      <input type="text" class="form-control" name="CGPA4" placeholder="Enter CGPA out of 4">
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div id="CGPA_four" style="display: none;">
                                      <input type="text" class="form-control" name="CGPA5" placeholder="Enter CGPA out of 5">
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div class="form-group">
                                <select name="degree" class="form-control">
                                  <option value="">Select</option>
                               <option value="Degree/Hons.">Degree/Hons.</option>
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <select name="degreegroup" class="form-control">
                                  <option value="">Select</option>
                                  <option value="Science">Science</option>
                                  <option value="Humanities">Humanities</option>
                                  <option value="Business Studies">Business Studies</option>
                                  <option value="Business Studies">Business Studies</option>
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <select name="degreeboard" class="form-control">
                                  <option value="">Select</option>
                                  <option value="Dhaka">Dhaka</option>
                                  <option value="Chittagong">Chittagong</option>
                                  <option value="Comilla">Comilla</option>
                                  <option value="Rajshahi">Rajshahi</option>
                                  <option value="Jessore">Jessore</option>
                                  <option value="Barishal">Barishal</option>
                                  <option value="Dinajpur">Dinajpur</option>
                                  <option value="Madrasah">Madrasah</option>
                                  <option value="Madrasah">Madrasah</option>
                                  <option value="Technical">Technical</option>
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <select name="degreepassingYear" class="form-control">
                                  <option value="">Select</option><option value="2019">2019</option>
                                  <option value="2018">2018</option><option value="2017">2017</option>
                                  <option value="2016">2016</option><option value="2015">2015</option>
                                  <option value="2014">2014</option><option value="2013">2013</option>
                                  <option value="2012">2012</option><option value="2011">2011</option>
                                  <option value="2010">2010</option><option value="2009">2009</option>
                                  <option value="2008">2008</option><option value="2007">2007</option>
                                  <option value="2006">2006</option><option value="2005">2005</option>
                                  <option value="2004">2004</option><option value="2003">2003</option>
                                  <option value="2002">2002</option><option value="2001">2001</option>
                                  <option value="2000">2000</option><option value="1999">1999</option>
                                  <option value="1998">1998</option><option value="1997">1997</option>
                                  <option value="1996">1996</option><option value="1995">1995</option>
                                  <option value="1994">1994</option><option value="1993">1993</option>
                                  <option value="1992">1992</option><option value="1991">1991</option>
                                  <option value="1999">1990</option>
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <div class="row">
                                  <div class="col-md-12">
                                    <select name="degreeresult" class="form-control" onchange="gpaFunctionC(this)">
                                      <option value="">Select</option>
                                      <option value="1st Division">1st Division</option>
                                      <option value="2nd Division">2nd Division</option>
                                      <option value="3rd Division">3rd Division</option>
                                      <option value="GPA (Out of 4)">GPA (Out of 4)</option>
                                      <option value="GPA (Out of 5)">GPA (Out of 5)</option>
                                    </select>
                                  </div>
                                  <div class="col-md-12">
                                    <div id="CGPA_five" style="display: none;">
                                      <input type="text" class="form-control" name="CGPA4" placeholder="Enter CGPA out of 4">
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div id="CGPA_six" style="display: none;">
                                      <input type="text" class="form-control" name="CGPA5" placeholder="Enter CGPA out of 5">
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </fieldset>
                    <fieldset class="part">
                      <h6 class="subtit">Experience Data:</h6>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="marit">
                              <label class="control-label">Work experience Certificate</label>
                              <label class="radio-inline">
                                <input type="radio" name="maritalStatus" value="Yes" required> Yes
                              </label>
                              <label class="radio-inline">
                                <input type="radio" name="maritalStatus" value="No" required> No
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6"></div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label class="control-label">Company Name</label>
                            <input  maxlength="100" name="companyName" type="text" class="form-control" placeholder="Company Name" required/>
                          </div>
                          <div class="col-md-6">
                            <label class="control-label">Company Address</label>
                            <input  maxlength="100" name="companyAddress" type="text" class="form-control" placeholder="Company Address" required/>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-4">
                            <label class="control-label">Type Of Work</label>
                            <input  maxlength="100" name="typeOfwork" type="text" class="form-control" placeholder="Type Of Work" required/>
                          </div>
                          <div class="col-md-4">
                            <label class="control-label">Work Duration</label>
                            <input  maxlength="100" name="workDuration" type="text" class="form-control" placeholder="Work Duration" required/>
                          </div>

                          <div class="col-md-4">
                            <label class="control-label">Months</label>
                            <input  maxlength="100" name="months" type="text" class="form-control" placeholder="Months" required/>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label class="control-label">Company Email ID</label>
                            <input  maxlength="100" name="comEmail" type="email" class="form-control" placeholder="Company Email ID" required/>
                          </div>
                          <div class="col-md-6">
                            <label class="control-label">Company Website</label>
                            <input  maxlength="100" name="comWebsite" type="text" class="form-control" placeholder="Company Website" required/>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <!--<a href="#step-1" type="button" class="btn btn-primary prevBtn btn-sm pull-left">Previous</a>-->
                        </div>
                        <div class="col-md-6">
                          <!--<button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button>-->
                          <button class="btn btn-success btn-sm pull-right" type="submit" name="submit">Submit</button>
                        </div>
                      </div>
                    </fieldset>
                  </div>
                       <div class="text-center"><a href="applicants.php" class="btn btn-primary" role="button">Go Back</a></div>
                </div>
              </div>

            </form>
ASSFORM;


?>
  <style>
    .stepwizard-step p {
      margin-top: 10px;
    }
    .stepwizard-row {
      display: table-row;
    }
    .stepwizard {
      display: table;
      width: 100%;
      position: relative;
    }
    .stepwizard-step button[disabled] {
      opacity: 1 !important;
      filter: alpha(opacity=100) !important;
    }
    .stepwizard-row:before {
      top: 14px;
      bottom: 0;
      position: absolute;
      content: " ";
      width: 100%;
      height: 1px;
      background-color: #ccc;
      z-order: 0;
    }
    .stepwizard-step {
      display: table-cell;
      text-align: center;
      position: relative;
    }
    .btn-circle {
      width: 30px;
      height: 30px;
      text-align: center;
      padding: 6px 0;
      font-size: 12px;
      line-height: 1.428571429;
      border-radius: 15px;
    }
    .part {
      border: 1px solid #337ab7;
      padding: 10px;
      margin-bottom: 10px;
      border-radius: 6px;
    }
    .subtit {
      background: #337ab7;
      color: #fff;
      padding: 3px;
      text-align: center;
    }
    .marit {
      padding-top: 29px;
    }
  </style>
  <div class="">
    <div class="container">
      <div class="row">

      </div>
    </div>
  </div>
  <!-- Start About area -->
  <div id="about" class="about-area">
    <div class="container">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="Mycon">

            <div class="stepwizard"><!--col-md-6 col-md-offset-3-->
              <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                  <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                  <p>Step 1</p>
                </div>
                <div class="stepwizard-step">
                  <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                  <p>Step 2</p>
                </div>
              </div>
            </div>

            <?php
            echo $assessment;
            ?>


          </div>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </div>

  <script>
    //step1 : refference others filed
    function myRef(that) {
      if (that.value == "Others") {
        //alert("check");
        document.getElementById("option").style.display = "block";
      } else {
        document.getElementById("option").style.display = "none";
      }
    }
    //step 1: passport data full name
    function FName(){
      var num1 = document.assessment.surName.value;
      var num2 = document.assessment.givenName.value;
      var space =" ";
      var sum =num1+space+num2;
      document.getElementById('Full_name').value = sum;
    }
    //step1 : Marital status with hidden spouse name
    function maritStatus(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("marit_option").style.display = "block";
      } else {
        document.getElementById("marit_option").style.display = "none";
      }
    }

    //step2 : CGPA Result
    function gpaFunctionA(that) {
      //gpa out of 4
      if (that.value == "GPA (Out of 4)") {

        document.getElementById("CGPA_one").style.display = "block";
      } else {
        document.getElementById("CGPA_one").style.display = "none";
      }
      //gpa out of 5
      if (that.value == "GPA (Out of 5)") {

        document.getElementById("CGPA_two").style.display = "block";
      } else {
        document.getElementById("CGPA_two").style.display = "none";
      }
    }
    function gpaFunctionB(that) {
      //gpa out of 4
      if (that.value == "GPA (Out of 4)") {

        document.getElementById("CGPA_three").style.display = "block";
      } else {
        document.getElementById("CGPA_three").style.display = "none";
      }
      //gpa out of 5
      if (that.value == "GPA (Out of 5)") {

        document.getElementById("CGPA_four").style.display = "block";
      } else {
        document.getElementById("CGPA_four").style.display = "none";
      }
    }
    function gpaFunctionC(that) {
      //gpa out of 4
      if (that.value == "GPA (Out of 4)") {

        document.getElementById("CGPA_five").style.display = "block";
      } else {
        document.getElementById("CGPA_five").style.display = "none";
      }
      //gpa out of 5
      if (that.value == "GPA (Out of 5)") {

        document.getElementById("CGPA_six").style.display = "block";
      } else {
        document.getElementById("CGPA_six").style.display = "none";
      }
    }

    //step3 : Before work country
    function beforeWork(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("workBefore").style.display = "block";
      } else {
        document.getElementById("workBefore").style.display = "none";
      }
    }
    //step3 : Embassy refused country
    function refuseEmbassy(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("refuseEmb").style.display = "block";
      } else {
        document.getElementById("refuseEmb").style.display = "none";
      }
    }
    //step3 : Last Passport
    function lastPassport(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("lastPass").style.display = "block";
      } else {
        document.getElementById("lastPass").style.display = "none";
      }
    }
    //step3 : Face cutting
    function face(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("fcecut").style.display = "block";
      } else {
        document.getElementById("fcecut").style.display = "none";
      }
    }
    //step3 : Passport copy
    function passCopy(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("pcopy").style.display = "block";
      } else {
        document.getElementById("pcopy").style.display = "none";
      }
    }
    //step3 : CV copy
    function cvCopy(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("cvcopies").style.display = "block";
      } else {
        document.getElementById("cvcopies").style.display = "none";
      }
    }
    //step3 : Birth Certificate
    function birthCert(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("bcert").style.display = "block";
      } else {
        document.getElementById("bcert").style.display = "none";
      }
    }
    //step3 : Birth Certificate
    function oldVisa(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("Oldvisa").style.display = "block";
      } else {
        document.getElementById("Oldvisa").style.display = "none";
      }
    }
    //step3 : NID Copy
    function NIDcopy(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("NIDc").style.display = "block";
      } else {
        document.getElementById("NIDc").style.display = "none";
      }
    }
    //step3 : picture 2 Copy
    function picTwocopy(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("picture").style.display = "block";
      } else {
        document.getElementById("picture").style.display = "none";
      }
    }
    //step3 : Police clearence
    function policeCert(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("policeClrcert").style.display = "block";
      } else {
        document.getElementById("policeClrcert").style.display = "none";
      }
    }
    //step3 : Work experience
    function workExpr(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("wexpr").style.display = "block";
      } else {
        document.getElementById("wexpr").style.display = "none";
      }
    }
    //step3 : Marriage certificate
    function marriageCert(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("marriage").style.display = "block";
      } else {
        document.getElementById("marriage").style.display = "none";
      }
    }
    //step3 :Bank Statement
    function bankStatement(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("Bstatement").style.display = "block";
      } else {
        document.getElementById("Bstatement").style.display = "none";
      }
    }
    //step3 : Air ticket reservation
    function airTicket(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("Airticket").style.display = "block";
      } else {
        document.getElementById("Airticket").style.display = "none";
      }
    }
    //step3 : Insurance
    function insuranceCopy(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("insCopy").style.display = "block";
      } else {
        document.getElementById("insCopy").style.display = "none";
      }
    }
    //step3 : Work condition
    function agreeToWork(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("wConditon").style.display = "block";
      } else {
        document.getElementById("wConditon").style.display = "none";
      }
    }


    //step4 : Hours per week/month
    function wrkingHour(that) {
      //gpa out of 4
      if (that.value == "[Hours per week] /Week") {

        document.getElementById("wrkWeek").style.display = "block";
      } else {
        document.getElementById("wrkWeek").style.display = "none";
      }
      //gpa out of 5
      if (that.value == "[Hours per month] /Month") {

        document.getElementById("wrkMonth").style.display = "block";
      } else {
        document.getElementById("wrkMonth").style.display = "none";
      }
    }
    //step 4: Salary per week/month
    function salaryHour(that) {
      //gpa out of 4
      if (that.value == "[Salary per week] /Week") {

        document.getElementById("salaryWeek").style.display = "block";
      } else {
        document.getElementById("salaryWeek").style.display = "none";
      }
      //gpa out of 5
      if (that.value == "[Salary per month] /Month") {

        document.getElementById("salaryMonth").style.display = "block";
      } else {
        document.getElementById("salaryMonth").style.display = "none";
      }
    }

    //step4 : Accomondation copy
    function accomCopy(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("accom").style.display = "block";
      } else {
        document.getElementById("accom").style.display = "none";
      }
    }
    //step4 : Resident copy
    function residntCopy(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("resident").style.display = "block";
      } else {
        document.getElementById("resident").style.display = "none";
      }
    }
    //step4 : Food by company
    function foodbyCompany(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("foodcopy").style.display = "block";
      } else {
        document.getElementById("foodcopy").style.display = "none";
      }
    }
    //step4 : Medical by company
    function medicalbyCompany(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("medicalcopy").style.display = "block";
      } else {
        document.getElementById("medicalcopy").style.display = "none";
      }
    }
    //step4 : Transport by company
    function transbyCompany(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("transcopy").style.display = "block";
      } else {
        document.getElementById("transcopy").style.display = "none";
      }
    }
    //step4 : Insurence by company
    function insrncebyCompany(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("insrncecopy").style.display = "block";
      } else {
        document.getElementById("insrncecopy").style.display = "none";
      }
    }
    //step4 : Terms & condition by company
    function termAndcondition(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("termCon").style.display = "block";
      } else {
        document.getElementById("termCon").style.display = "none";
      }
    }



    //step5 : Check Option [Received email copy]
    $(function () {
      $("#chk1").click(function () {
        if ($(this).is(":checked")) {
          $("#wpermitcopy").show();
        } else {
          $("#wpermitcopy").hide();
        }
      });
    });


    /*
     function checkA(that) {
     if (that.value == "Work Permit") {
     //alert("check");
     document.getElementById("wpermitcopy").style.display = "block";
     } else {
     document.getElementById("wpermitcopy").style.display = "none";
     }
     }
     function checkB(that) {
     if (that.value == "Accommodation Letter") {
     //alert("check");
     document.getElementById("accomLetter").style.display = "block";
     } else {
     document.getElementById("accomLetter").style.display = "none";
     }
     }
     function checkC(that) {
     if (that.value == "Guarantee Letter") {
     //alert("check");
     document.getElementById("gaurnteeLetter").style.display = "block";
     } else {
     document.getElementById("gaurnteeLetter").style.display = "none";
     }
     }
     function checkD(that) {
     if (that.value == "Others") {
     //alert("check");
     document.getElementById("wothers").style.display = "block";
     } else {
     document.getElementById("wothers").style.display = "none";
     }
     }
     function checkE(that) {
     if (that.value == "Contract Paper") {
     //alert("check");
     document.getElementById("wcontractcopy").style.display = "block";
     } else {
     document.getElementById("wcontractcopy").style.display = "none";
     }
     }
     function checkF(that) {
     if (that.value == "Extention Letter") {
     //alert("check");
     document.getElementById("wextnLetter").style.display = "block";
     } else {
     document.getElementById("wextnLetter").style.display = "none";
     }
     }
     function checkG(that) {
     if (that.value == "KRS Paper") {
     //alert("check");
     document.getElementById("krsPaper").style.display = "block";
     } else {
     document.getElementById("krsPaper").style.display = "none";
     }
     }

     */


    //step5 : Contract Paper
    function conPaper(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("conpaper").style.display = "block";
      } else {
        document.getElementById("conpaper").style.display = "none";
      }
    }
    //step5 : Accommodation Paper
    function AccPaper(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("Accomopaper").style.display = "block";
      } else {
        document.getElementById("Accomopaper").style.display = "none";
      }
    }
    //step5 : Guarantee Paper
    function GuarnteePaper(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("Gaurpaper").style.display = "block";
      } else {
        document.getElementById("Gaurpaper").style.display = "none";
      }
    }
    //step5 : Extension Paper
    function ExtPaper(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("Ext").style.display = "block";
      } else {
        document.getElementById("Ext").style.display = "none";
      }
    }
    //step5 : KRS Paper
    function KRSPaper(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("KRS").style.display = "block";
      } else {
        document.getElementById("KRS").style.display = "none";
      }
    }
    //step5 : Company tax payment Paper
    function compTaxPaper(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("comTax").style.display = "block";
      } else {
        document.getElementById("comTax").style.display = "none";
      }
    }
    //step6 : Visa copy
    function visaCopy(that) {
      if (that.value == "Yes") {
        //alert("check");
        document.getElementById("visacopy").style.display = "block";
      } else {
        document.getElementById("visacopy").style.display = "none";
      }
    }

  </script>

  <!-- End About area -->

<?php
include('footer.php');
?>