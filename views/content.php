<?php
use App\Utility\Utility;

if($_SESSION['loginas']=='Admin'){
	$allmembers=$objController->view($_GET);
	//   var_dump($allmembers);
}else{
	Utility::redirect('home.php');
}
//var_dump($_SESSION); die();

?>
    <!-- Page Content -->
    <div class="container">
    	<div class="">
<style>
	#membersview th,td{
		text-align: center;
	}
</style>
		<table id="membersview" border="1" width="100%" style="border-collapse: collapse; ">
			<thead>
			<tr>
				<th>ID</th><th>Photo</th><th>Name</th><th>DOB</th><th>Mobile</th><th>Actions</th>
			</tr>
			</thead>

			<?php
			$assessment;
			foreach($allmembers as $singleMember){
				if($singleMember->assessment=='Yes'){ $assessment=" ";}
				else{$assessment="<a class='btn btn-success' href='assessment.php?email=$singleMember->email'>Assessment</a>"; }
				echo "
				<tr><td>$singleMember->id</td><td><img width='70px' src=\"uploads/$singleMember->picture\" class=\"img-circle\"></td><td>$singleMember->fullName</td><td>$singleMember->dob</td><td>$singleMember->phoneNumber</td>

				<td>
				<a class='btn btn-primary' href='profile.php?email=$singleMember->email'>View</a>
				<a class='btn btn-warning' href='edit.php?email=$singleMember->email'>Edit</a> ".
				$assessment
				." <a class='btn btn-danger' href='delete.php?deleteid=$singleMember->id'>Delete</a>
				</td>
			</tr>
				";
			}


			?>


		</table>

		</div>
	</div>	
    
