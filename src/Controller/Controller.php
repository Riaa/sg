<?php
namespace App\Controller;
use App\Message\Message;
use App\Utility\Utility;
use App\Rbac\Role;
use App\Rbac\PrivilegedUser;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Controller extends  DB{

 private  $id,$asfor,$fileNo,$refEm,$refSkygoal,$consltName,$Applycountry,$toWork,$surName,$givenName,$fullName,$fathersName,$mothersName,$maritalStatus,$dob,$age,$pzcode,$pob,$citizenship,$passportNo,$passportNoOld,$placeOfissue,$passportIssuedate,$passportExpirydate,$NID,$permAddress1,$permAddress2,$post_office,$zip_code,$state,$country,$degree_name1,$group1,$board1,$passingYear1,$result1,$companyName,$companyAddress,$typeOfwork,$workDuration,$months,$comEmail,$comWebsite,$fatherName, $motherName,$addressLine1, $addressLine2, $city,$postcode,$email,$phoneNumber,$gender,$tov, $passport, $birthcert, $passport_file, $picture, $brc, $cv, $comment, $tmpPass, $singleView,$deleteid,$last_name,$address,$phone;

    public function setData($postData){

        //var_dump($postData);
        if(array_key_exists('password',$postData)){$this->password=md5($postData['password']);}
        if(array_key_exists('asfor',$postData)){$this->asfor=$postData['asfor'];}
        if(array_key_exists('fileNo',$postData)){$this->fileNo=$postData['fileNo'];}
        if(array_key_exists('refEm',$postData)){$this->refEm=$postData['refEm'];}
        if(array_key_exists('refSkygoal',$postData)){$this->refSkygoal=$postData['refSkygoal'];}
        if(array_key_exists('consltName',$postData)){$this->consltName=$postData['consltName'];}
        if(array_key_exists('Applycountry',$postData)){$this->Applycountry=$postData['Applycountry'];}
        if(array_key_exists('toWork',$postData)){$this->toWork=$postData['toWork'];}
        if(array_key_exists('surName',$postData)){$this->surName=$postData['surName'];}
        if(array_key_exists('givenName',$postData)){$this->givenName=$postData['givenName'];}
        if(array_key_exists('fullName',$postData)){$this->fullName=$postData['fullName'];}
        if(array_key_exists('fathersName',$postData)){$this->fathersName=$postData['fathersName'];}
        if(array_key_exists('mothersName',$postData)){$this->mothersName=$postData['mothersName'];}
        if(array_key_exists('maritalStatus',$postData)){$this->maritalStatus=$postData['maritalStatus'];}
        if(array_key_exists('dob',$postData)){$this->dob=$postData['dob'];}
        if(array_key_exists('age',$postData)){$this->age=$postData['age'];}
        if(array_key_exists('pzcode',$postData)){$this->pzcode=$postData['pzcode'];}
        if(array_key_exists('pob',$postData)){$this->pob=$postData['pob'];}
        if(array_key_exists('citizenship',$postData)){$this->citizenship=$postData['citizenship'];}
        if(array_key_exists('passportNo',$postData)){$this->passportNo=$postData['passportNo'];}
        if(array_key_exists('passportNoOld',$postData)){$this->passportNoOld=$postData['passportNoOld'];}
        if(array_key_exists('placeOfissue',$postData)){$this->placeOfissue=$postData['placeOfissue'];}
        if(array_key_exists('passportIssuedate',$postData)){$this->passportIssuedate=$postData['passportIssuedate'];}
        if(array_key_exists('passportExpirydate',$postData)){$this->passportExpirydate=$postData['passportExpirydate'];}
        if(array_key_exists('NID',$postData)){$this->NID=$postData['NID'];}
        if(array_key_exists('permAddress1',$postData)){$this->permAddress1=$postData['permAddress1'];}
        if(array_key_exists('permAddress2',$postData)){$this->permAddress2=$postData['permAddress2'];}
        if(array_key_exists('post_office',$postData)){$this->post_office=$postData['post_office'];}
        if(array_key_exists('zip_code',$postData)){$this->zip_code=$postData['zip_code'];}
        if(array_key_exists('ssc',$postData)){$this->ssc=$postData['ssc'];}
        if(array_key_exists('hsc',$postData)){$this->hsc=$postData['hsc'];}
        if(array_key_exists('degree',$postData)){$this->degree=$postData['degree'];}
        if(array_key_exists('maritalStatus',$postData)){$this->maritalStatus=$postData['maritalStatus'];}
        if(array_key_exists('companyName',$postData)){$this->companyName=$postData['companyName'];}
        if(array_key_exists('companyAddress',$postData)){$this->companyAddress=$postData['companyAddress'];}
        if(array_key_exists('typeOfwork',$postData)){$this->typeOfwork=$postData['typeOfwork'];}
        if(array_key_exists('workDuration',$postData)){$this->workDuration=$postData['workDuration'];}
        if(array_key_exists('months',$postData)){$this->months=$postData['months'];}
        if(array_key_exists('comEmail',$postData)){$this->comEmail=$postData['comEmail'];}
        if(array_key_exists('comWebsite',$postData)){$this->comWebsite=$postData['comWebsite'];}


        if(array_key_exists('fullName',$postData)){$this->fullName=$postData['fullName'];}
        if(array_key_exists('fatherName',$postData)){$this->fatherName =$postData['fatherName']; }
        if(array_key_exists('motherName',$postData)){$this->motherName = $postData['motherName']; }
        if(array_key_exists('addressLine1',$postData)){$this->addressLine1 = $postData['addressLine1']; }
        if(array_key_exists('addressLine2',$postData)){ $this->addressLine2 = $postData['addressLine2']; }
        if(array_key_exists('city',$postData)){ $this->city = $postData['city'];}
        if(array_key_exists('state',$postData)){$this->state = $postData['state'];}
        if(array_key_exists('postcode',$postData)){$this->postcode = $postData['postcode'];}
        if(array_key_exists('country',$postData)){$this->country = $postData['country'];}
        if(array_key_exists('email',$postData)){$this->email = $postData['email'];}
        if(array_key_exists('phoneNumber',$postData)){$this->phoneNumber = $postData['phoneNumber'];}
        if(array_key_exists('dob',$postData)){$this->dob = $postData['dob'];}
        if(array_key_exists('gender',$postData)){$this->gender = $postData['gender'];}
        if(array_key_exists('tov',$postData)){$this->tov = $postData['tov'];}
        if(array_key_exists('passport',$postData)){$this->passport= $postData['passport'];}
        if(array_key_exists('birthcert',$postData)){$this->birthcert = $postData['birthcert'];}
        if(array_key_exists('passport_file',$postData)){$this->passport_file = $postData['passport_file'];}
        if(array_key_exists('picture',$postData)){$this->picture = $postData['picture'];}
        if(array_key_exists('brc',$postData)){$this->brc = $postData['brc'];}
        if(array_key_exists('cv',$postData)){$this->cv = $postData['cv'];}
        if(array_key_exists('comments',$postData)){$this->comment = $postData['comments'];}
        if(array_key_exists('singleView',$postData)){$this->singleView = $postData['singleView'];}
        if(array_key_exists('deleteid',$postData)){$this->deleteid = $postData['deleteid'];}
        if(array_key_exists('id',$postData)){$this->id = $postData['id'];}
        if(array_key_exists('user_id',$postData)){$this->id = $postData['user_id'];}

    }
    public function objectToArray($objectData){
        $objectToArray = json_decode(json_encode($objectData), True);
        return $objectToArray;
    }
    public function generate_string($input, $strength = 16){
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
        return $random_string;
     }
    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+';
        $pass = array(); //remember to declare $pass as an array
        $tmpPass=$pass;
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    public function sms($mobileNumber,$message){

        ################ Getting SMS settings from DB ####################################
        $sql="SELECT * FROM settings WHERE name='sms'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $smsSettings=$STH->fetch();
        $objToArraySmsSet = json_decode(json_encode($smsSettings), True);
        ################ Getting SMS settings from DB Ended ###############################
        $user = $objToArraySmsSet['username'];
        $password = $objToArraySmsSet['password'];
        $sender = $objToArraySmsSet['senderid'];
        $url = "http://api.mimsms.com/api/v3/sendsms/plain?user=$user&password=$password&sender=$sender&SMSText=$message&GSM=$mobileNumber&";
        $smsResult = simplexml_load_file($url);
        return $smsResult;

    }
    public function store($storeData){

            $Pass=$this->randomPassword();

            $this->tmpPass=$Pass;

            $this->password=md5($Pass);

        $sql =" ";
        $arrData ="";
             $arrData = array($this->password,$this->fullName,$this->fatherName,$this->motherName,$this->addressLine1,$this->addressLine2,$this->city,$this->state,$this->postcode,$this->country,$this->email,$this->phoneNumber,$this->dob,$this->gender,$this->tov,$this->passport,$this->birthcert,$this->passport_file,$this->picture,$this->brc,$this->cv,$this->comment);
        $sql = "INSERT INTO members(password,fullName,fatherName, motherName,addressLine1, addressLine2, city, state, postcode, country,email, phoneNumber, dob, gender,tov, passport, birthcert, passport_file, picture, brc, cv, comment) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


        if(isset($storeData['add']) && $storeData['add']=='addstaff') {
            $this->password=md5($storeData['password']);
            $arrData = array($this->password,$this->fullName,$this->addressLine1,$this->phoneNumber,$this->email);
            $sql = "INSERT INTO users(password,last_name,address,phone,email) VALUES (?,?,?,?,?)";
            $this->redirect = 'staff.php?admin=allstaff';
      // var_dump($arrData);
        }


        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("Success! You have  successfully registered:)");
       }
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");
        Utility::redirect("$this->redirect");


    }
    public function assessment(){

        $arrData = array($this->id,$this->asfor,$this->fileNo,$this->refEm,$this->refSkygoal,$this->consltName,$this->Applycountry,$this->toWork);
        //var_dump( $arrData) ; die();

        $sql = "INSERT INTO assessment(mid,asfor,fileNo,refEm,refSkygoal,consltName,Applycountry,toWork) VALUES (?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
    }
    public function passportdata(){

        $arrData = array($this->id,$this->surName,$this->givenName,$this->fullName,$this->fathersName,$this->mothersName,$this->maritalStatus,$this->dob,$this->age,$this->pzcode,$this->pob,$this->citizenship,$this->passportNo,$this->passportNoOld,$this->placeOfissue,$this->passportIssuedate,$this->passportExpirydate,$this->NID,$this->permAddress1,$this->permAddress2,$this->post_office,$this->zip_code,$this->state,$this->country);
        //var_dump( $arrData) ; die();

        $sql = "INSERT INTO passportdata(mid,surName,givenName,fullName,fathersName,mothersName,maritalStatus,dob,age,pzcode,pob,citizenship,passportNo,passportNoOld,placeOfissue,passportIssuedate,passportExpirydate,NID,permAddress1,permAddress2,post_office,zip_code,state,country) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
    }
    public function educationdata(){

        $arrData = array($this->id,$this->ssc,$this->hsc,$this->degree);
        //var_dump( $arrData) ; die();

        $sql = "INSERT INTO educationaldata(mid,ssc,hsc,degree) VALUES (?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
    }
    public function experiencedata(){

        $arrData = array($this->id,$this->companyName,$this->companyAddress,$this->typeOfwork,$this->workDuration,$this->months,$this->comEmail,$this->comWebsite);
        //var_dump( $arrData) ; die();

        $sql = "INSERT INTO experiencedata(mid,companyName,companyAddress,typeOfwork,workDuration,months,comEmail,comWebsite) VALUES (?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
    }
    public function assstore(){

            $sql="UPDATE members SET assessment='Yes' WHERE id=$this->id";
            $result =$this->DBH->exec($sql);
            if($result){
                $this->assessment();
                $this->passportdata();
                $this->educationdata();
                $this->experiencedata();
                Message::message("Success! You have
                successfully registered:)");
            }
        else{
            Message::message("Failed! Data Has Not Been Inserted :( ");
        }

        Utility::redirect('home.php');
    }
    public function fileUpload($postData,$filesData){
    if(isset($postData)){
    if(count($_FILES['fileToUpload']['name']) > 0){
        $totalFiles=count($_FILES['fileToUpload']['name']);
        for($i=0; $i<$totalFiles; $i++) {
            $tmpFilePath = $_FILES['fileToUpload']['tmp_name'][$i];
            if($tmpFilePath != ""){
                 $shortname = time().$_FILES['fileToUpload']['name'][$i];
                if($i==0){
                    $_POST['passport_file']="passport".$_POST['phoneNumber']. $shortname;
                    $fileName=$_POST['passport_file']; }
                if($i==1){
                    $_POST['picture']="picture".$_POST['phoneNumber'].$shortname;
                    $fileName=$_POST['picture']; }
                if($i==2){
                    $_POST['brc']="brc".$_POST['phoneNumber']. $shortname;
                    $fileName=$_POST['brc']; }
                if($i==3){
                    $_POST['cv']="cv".$_POST['phoneNumber']. $shortname;
                    $fileName=$_POST['cv']; }
                 $filePath = "uploads/" .$fileName;
                if(move_uploaded_file($tmpFilePath, $filePath)) {
                    $files[] = $shortname;
                    $listFile=count($files);
                }
            }
        }
        if($listFile){
            $this->setData($_POST);
            if($_POST['update']=='update'){
                $this->update();
                Utility::redirect('home.php');
            }else{
                $this->store();
                Utility::redirect('email.php?fullName='.$this->fullName.'&password='.$this->tmpPass.'&email='.$this->email.'&sendmail=ok');
            }


        }else{
            unset($files,$listFile); return True;
        }

        }

    }
}
    public function view($viewData) {
//var_dump($viewData); die();
        $sql = "";

        $sql = "SELECT * FROM  members WHERE soft_delete='No'";
        if(isset($viewData['member'])){
            $sql = "SELECT * FROM  members WHERE assessment='Yes' AND soft_delete='No'";}
        else if($_SESSION['loginas']=='User' || $this->singleView=='Yes'){
          $sql = "SELECT * FROM  members WHERE email='$this->email' AND soft_delete='No'";
            if(isset($viewData['admin']) && $viewData['admin']=='admin') {
              $this->email=$_SESSION['email'];
              $sql = "SELECT * FROM  users WHERE email='$this->email'";
          }
            if(isset($viewData['admin']) && $viewData['admin']=='staff') {

                $sql = "SELECT * FROM  users WHERE email='$this->email'";
            }
            if(isset($viewData['admin']) && $viewData['admin']=='allstaff') {
              $this->email=$_SESSION['email'];
              $sql = "SELECT * FROM  users ";
          }
      }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function update($updateData){
        $arrData="";
        $sql="";

        $arrData = array($this->fullName,$this->fatherName,$this->motherName,$this->addressLine1,$this->addressLine2,$this->city,$this->state,$this->postcode,$this->country,$this->phoneNumber,$this->dob,$this->gender,$this->tov,$this->passport,$this->birthcert,$this->passport_file,$this->picture,$this->brc,$this->cv,$this->comment);
        //
        //
        $sql = "UPDATE members SET  fullName=?,fatherName=?, motherName=?,addressLine1=?, addressLine2=?, city=?, state=?, postcode=?, country=?, phoneNumber=?, dob=?, gender=?,tov=?, passport=?, birthcert=?, passport_file=?, picture=?, brc=?, cv=?, comment=? WHERE email='$this->email'";
        $this->redirect = 'home.php';

        if(isset($updateData['edit']) && $updateData['edit']=='editstaff') {
            //var_dump($updateData); die();
            $arrData = array($this->password,$this->fullName,$this->addressLine1,$this->phoneNumber,$this->email);
            $sql = "UPDATE users SET   password=?, last_name=?,address=?, phone=?, email=? WHERE user_id='$this->id'";
            $this->redirect = 'staff.php?admin=allstaff';

           // var_dump($arrData);
        }

            //var_dump($this->DBH);die();
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success!");
        else
            Message::message("Failed!");

        Utility::redirect("$this->redirect");
    }
    public function delete(){

        $sql ="";

        $sql = "Update  members set soft_delete='Yes' WHERE id=".$this->deleteid;

        if(isset($deleteData['deleteid']) && $_SESSION['loginas']=='Admin'){
            $sql = "DELETE FROM users WHERE user_id=".$this->deleteid;
            echo "Delete"; die();
        }
        $result = $this->DBH->exec($sql);

        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");
        Utility::redirect('home.php');
    }
    public function deletestaff(){
            $sql = "DELETE FROM users WHERE user_id=".$this->deleteid;
            //echo "Delete"; die();
        $result = $this->DBH->exec($sql);

        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");
        Utility::redirect('staff.php?admin=allstaff');
    }

}