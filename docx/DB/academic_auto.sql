-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 22, 2019 at 04:02 AM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `academic_auto`
--

-- --------------------------------------------------------

--
-- Table structure for table `assessment`
--

CREATE TABLE IF NOT EXISTS `assessment` (
  `id` int(11) NOT NULL,
  `mid` int(3) DEFAULT NULL,
  `asfor` varchar(30) NOT NULL,
  `fileno` int(30) NOT NULL,
  `refem` varchar(30) NOT NULL,
  `refskygoal` varchar(30) NOT NULL,
  `consltname` varchar(30) NOT NULL,
  `applycountry` varchar(30) NOT NULL,
  `towork` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment`
--

INSERT INTO `assessment` (`id`, `mid`, `asfor`, `fileno`, `refem`, `refskygoal`, `consltname`, `applycountry`, `towork`) VALUES
(1, 29, 'asd', 765, 'adna', 'Skygoal Synergy', 'asdfg', 'Czech Republic', 'staff'),
(2, 29, 'sdf', 765, 'adna', 'Skygoal Synergy', 'asdfg', 'Estonia', 'staff'),
(3, 28, 'sdf', 765, 'adna', 'Skygoal Synergy', 'asdfg', 'Estonia', 'staff'),
(4, 28, 'asd', 765, 'adna', 'Saifur Rahman Chowddhury', 'asdfg', 'Poland', 'staff');

-- --------------------------------------------------------

--
-- Table structure for table `educationaldata`
--

CREATE TABLE IF NOT EXISTS `educationaldata` (
  `id` int(11) NOT NULL,
  `mid` int(3) DEFAULT NULL,
  `ssc` varchar(100) NOT NULL,
  `hsc` varchar(100) NOT NULL,
  `degree` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `educationaldata`
--

INSERT INTO `educationaldata` (`id`, `mid`, `ssc`, `hsc`, `degree`) VALUES
(1, 29, 'Science,Chittagong,2018,1st Division,,', 'Science,Dhaka,2018,1st Division,,', 'Science,Technical,2009,1st Division,,'),
(2, 29, 'Science,Dhaka,2017,2nd Division,,', 'Science,Technical,2009,1st Division,,', 'Science,Madrasah,2007,1st Division,,'),
(3, 28, 'Science,Dhaka,2019,1st Division,,', 'Science,Technical,2009,1st Division,,', 'Science,Technical,2008,1st Division,,'),
(4, 28, 'Science,Dhaka,2019,2nd Division,,', 'Science,Dhaka,2018,3rd Division,,', 'Science,Dhaka,2016,1st Division,,');

-- --------------------------------------------------------

--
-- Table structure for table `experiencedata`
--

CREATE TABLE IF NOT EXISTS `experiencedata` (
  `id` int(11) NOT NULL,
  `mid` int(3) DEFAULT NULL,
  `maritalstatus` varchar(30) NOT NULL,
  `companyname` varchar(30) NOT NULL,
  `companyaddress` varchar(30) NOT NULL,
  `typeofwork` varchar(30) NOT NULL,
  `workduration` varchar(30) NOT NULL,
  `months` varchar(30) NOT NULL,
  `comemail` varchar(30) NOT NULL,
  `comwebsite` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `experiencedata`
--

INSERT INTO `experiencedata` (`id`, `mid`, `maritalstatus`, `companyname`, `companyaddress`, `typeofwork`, `workduration`, `months`, `comemail`, `comwebsite`) VALUES
(1, 29, '', 'erftgyhuj', 'asdfghj', 'asdfgh', '23456', '2345', 'asdfgh@gmail.com', 'asdfghj'),
(2, 29, '', 'erftgyhuj', 'asdfghj', 'asdfgh', '23456', '2345', 'asdfgh@gmail.com', 'asdfghj'),
(3, 28, '', 'erftgyhuj', 'asdfghj', 'asdfgh', '23456', '2345', 'asdfgh@gmail.com', 'asdfghj'),
(4, 28, '', 'erftgyhuj', 'asdfghj', 'asdfgh', '23456', '2345', 'asdfgh@gmail.com', 'asdfghj');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL,
  `password` varchar(200) DEFAULT NULL,
  `fullName` varchar(200) DEFAULT NULL,
  `fatherName` varchar(200) DEFAULT NULL,
  `motherName` varchar(200) DEFAULT NULL,
  `addressLine1` varchar(300) DEFAULT NULL,
  `addressLine2` varchar(300) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `country` varchar(15) DEFAULT NULL,
  `email` varchar(40) NOT NULL,
  `phoneNumber` varchar(15) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `passport` varchar(30) DEFAULT NULL,
  `birthcert` varchar(30) DEFAULT NULL,
  `tov` varchar(10) NOT NULL,
  `passport_file` varchar(150) DEFAULT NULL,
  `picture` varchar(150) DEFAULT NULL,
  `brc` varchar(150) DEFAULT NULL,
  `cv` varchar(150) DEFAULT NULL,
  `comment` varchar(300) DEFAULT NULL,
  `assessment` varchar(3) NOT NULL DEFAULT 'No',
  `soft_delete` varchar(3) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `password`, `fullName`, `fatherName`, `motherName`, `addressLine1`, `addressLine2`, `city`, `state`, `postcode`, `country`, `email`, `phoneNumber`, `dob`, `gender`, `passport`, `birthcert`, `tov`, `passport_file`, `picture`, `brc`, `cv`, `comment`, `assessment`, `soft_delete`) VALUES
(28, '0f339fc9bd8fd494384d29ba96e543a0', 'Mahamuda', 'Didarul Alam', 'Tanu', 'ctg', 'ctg', 'chittagong', 'N/A', '123', 'AFG', 'mahamuda@gmail.com', '01872222222', '2019-05-28', 'Female', '098765', '0987654323456789', 'Student Vi', 'passport018722222221560786658IMG-20190613-WA0007.jpg', 'picture018722222221560786658IMG-20190613-WA0007.jpg', 'brc018722222221560786658IMG-20190613-WA0007.jpg', 'cv018722222221560786658IMG-20190613-WA0007.jpg', 'ok', 'Yes', 'No'),
(29, 'ec2f9fd281dfb29e4c6d3a22d561b6e0', 'Smrity', 'Didarul Alam', 'Tanu', 'ctg', 'ctg', 'chittagong', 'N/A', '123', 'AZE', 'srity@gmail.com', '01647574161', '2019-05-27', 'Female', '09876543', '0987654323456789', 'Student Vi', 'passport016475741611560786811FB_IMG_1560785990236.jpg', 'picture016475741611560786811FB_IMG_1560785990236.jpg', 'brc016475741611560786811FB_IMG_1560785990236.jpg', 'cv016475741611560786811FB_IMG_1560785990236.jpg', 'ok', 'No', 'No'),
(30, '8b673c7595b2248f0b1e3d69c72494ad', 'Marjahan', 'Didarul Alam', 'Tanu', 'ctg', 'ctg', 'chittagong', 'N/A', '123', 'AFG', 'marjahan@gmail.com', '01647574161', '2019-05-27', 'Female', '09876543', '0987654323456789', 'Student Vi', 'passport016475741611560786898received_349425728972749.jpeg', 'picture016475741611560786898received_349425728972749.jpeg', 'brc016475741611560786898received_349425728972749.jpeg', 'cv016475741611560786898received_349425728972749.jpeg', 'ok', 'Yes', 'No'),
(31, '5527d653402b0fdda87f8191a0ccb1df', 'Fowziya', 'Didarul Alam', 'Tanu', 'ctg', 'ctg', 'chittagong', 'N/A', '123', 'AFG', 'fowziya@gmail.com', '01872222222', '2019-05-27', 'Female', '098765', '0987654323456789', 'Student Vi', 'passport018722222221560786973IMG-20190530-WA0017.jpg', 'picture018722222221560786973IMG-20190530-WA0017.jpg', 'brc018722222221560786973IMG-20190530-WA0017.jpg', 'cv018722222221560786973IMG-20190530-WA0017.jpg', 'ok', 'Yes', 'No'),
(33, '9bece596d54b3cf4010b79d75ac5f52a', 'Asmana ria', 'Anwar', 'Rina', 'ctg', 'ctg', 'chittagong', 'N/A', '123', 'AFG', 'ria2@gmail.com', '01872222222', '2019-06-05', 'Female', '098765', '0987654323456789', 'Student Vi', 'passport018722222221560787137IMG-20190605-WA0056.jpg', 'picture018722222221560787137IMG-20190605-WA0056.jpg', 'brc018722222221560787137IMG-20190605-WA0056.jpg', 'cv018722222221560787137IMG-20190605-WA0056.jpg', 'ok', 'NO', 'No'),
(37, 'd88dae2df93bbdd975c5129d915ce115', 'Asmana', 'Anwar', 'Rina', 'ctg', 'ctg', 'chittagong', 'N/A', '123', NULL, 'asmanaria6666@gmail.com', '01647574161', '2019-05-27', 'Female', '123456', '0987654323456789', 'Student Vi', 'passport01647574161156078968620190611_202627.jpg', 'picture01647574161156078968620190611_202627.jpg', 'brc01647574161156078968620190611_202627.jpg', 'cv01647574161156078968620190611_202627.jpg', 'ok', 'No', 'No'),
(38, '8af82327b913caa0cf72dc9b182da706', 'Asmana riya', 'Anwar', 'Rina', 'ctg', 'ctg', 'chittagong', 'N/A', '123', 'AFG', 'asmanaria66@gmail.com', '098765432', '2019-06-11', 'Female', '098765', '0987654323456789', 'Student Vi', 'passport098765432156083002020190611_202610.jpg', 'picture098765432156083002020190611_202627.jpg', 'brc098765432156083002020190611_202627.jpg', 'cv098765432156083002020190611_202627.jpg', 'ok', 'No', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) NOT NULL,
  `userType` varchar(10) NOT NULL,
  `menu` varchar(2048) NOT NULL,
  `softdelete` varchar(3) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `userType`, `menu`, `softdelete`) VALUES
(1, 'Admin', '<li class="active"> <a class="page-scroll" href="home.php">Home</a> </li>\n                 \n      <li>\n                    <a class="page-scroll" href="register.php">Members</a>\n                    <ul class="dropdown-menu">\n                    <li><a class="page-scroll" href="register.php">All Applicants</a></li>\n      				<li> <a class="page-scroll" href="register.php">Assessed Applicants</a>	</li>\n\n                    </ul>\n      </li>           \n      \n      <li>\n                    <a class="page-scroll" href="register.php">Register Online</a>\n      </li>\n      <li>\n                    <a class="page-scroll" href="#">Settings</a>\n                    <ul class="dropdown-menu">\n                         <li><a class="page-scroll" href="admin.php">Profile</a></li>\n                         <li><a class="page-scroll" href="createUser.php">Create User</a></li>\n                    </ul>\n      </li>', 'No'),
(2, 'User', ' <li class="active">\n                    <a class="page-scroll" href="index.php">Home</a>\n                  </li>        ', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `passportdata`
--

CREATE TABLE IF NOT EXISTS `passportdata` (
  `id` int(11) NOT NULL,
  `mid` int(3) DEFAULT NULL,
  `surname` varchar(30) NOT NULL,
  `givenname` varchar(30) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `fathersname` varchar(30) NOT NULL,
  `mothersname` varchar(30) NOT NULL,
  `maritalstatus` varchar(30) NOT NULL,
  `dob` date NOT NULL,
  `age` int(30) NOT NULL,
  `pzcode` int(30) NOT NULL,
  `pob` int(30) NOT NULL,
  `citizenship` varchar(30) NOT NULL,
  `passportno` int(30) NOT NULL,
  `passportnoold` int(30) NOT NULL,
  `placeofissue` varchar(30) NOT NULL,
  `passportissuedate` varchar(30) NOT NULL,
  `passportexpirydate` varchar(30) NOT NULL,
  `nid` int(30) NOT NULL,
  `permaddress1` varchar(30) NOT NULL,
  `permaddress2` varchar(30) NOT NULL,
  `post_office` varchar(30) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `country` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `passportdata`
--

INSERT INTO `passportdata` (`id`, `mid`, `surname`, `givenname`, `fullname`, `fathersname`, `mothersname`, `maritalstatus`, `dob`, `age`, `pzcode`, `pob`, `citizenship`, `passportno`, `passportnoold`, `placeofissue`, `passportissuedate`, `passportexpirydate`, `nid`, `permaddress1`, `permaddress2`, `post_office`, `zip_code`, `state`, `country`) VALUES
(1, 29, 'Smrity', 'Smrity', 'Smrity', 'Didarul Alam', 'Tanu', 'No', '2019-05-27', 10, 123, 2019, 'The Maldives', 9876543, 9876543, 'Dhaka', '2019-07-30', '2019-07-23', 2147483647, 'ctg', 'ctg', 'sdfgh', '123', 'N/A', 'AZE'),
(2, 29, 'Smrity', 'Smrity', 'Smrity', 'Didarul Alam', 'Tanu', 'No', '2019-05-27', 56, 123, 2019, 'India', 9876543, 9876543, 'Dhaka', '2019-07-30', '2019-07-29', 2147483647, 'ctg', 'ctg', 'sdfgh', '123', 'N/A', 'AZE'),
(3, 28, 'Mahamuda', 'Mahamuda', 'Mahamuda', 'Didarul Alam', 'Tanu', 'No', '2019-05-28', 45, 123, 2019, 'Bangladesh', 98765, 98765, 'State', '2019-07-30', '2019-07-23', 2147483647, 'ctg', 'ctg', 'sdfgh', '123', 'N/A', 'AFG'),
(4, 28, 'Mahamuda', 'Mahamuda', 'Mahamuda', 'Didarul Alam', 'Tanu', 'No', '2019-05-28', 23, 123, 2019, 'Bangladesh', 98765, 98765, 'Mofa', '2019-07-10', '2019-07-12', 2147483647, 'ctg', 'ctg', 'sdfgh', '123', 'N/A', 'AFG');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `soft_delete` varchar(3) NOT NULL DEFAULT 'No',
  `email_verified` varchar(111) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `soft_delete`, `email_verified`) VALUES
(3, '', 'Asmana ', 'asmana@olineit.com', 'asmana', '01714130077', 'ctg', 'No', 'Yes'),
(4, '', 'Sharmin', 'shar_28_min@gmail.com', 'asmana', '0183332246', 'Halishahar', 'No', NULL),
(5, '', 'Asmana ', 'asmanaria@gmail.com', 'd88dae2df93bbdd975c5129d915ce115', '01714130077', 'ctg', 'No', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assessment`
--
ALTER TABLE `assessment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mid` (`mid`);

--
-- Indexes for table `educationaldata`
--
ALTER TABLE `educationaldata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mid` (`mid`);

--
-- Indexes for table `experiencedata`
--
ALTER TABLE `experiencedata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mid` (`mid`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passportdata`
--
ALTER TABLE `passportdata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mid` (`mid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assessment`
--
ALTER TABLE `assessment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `educationaldata`
--
ALTER TABLE `educationaldata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `experiencedata`
--
ALTER TABLE `experiencedata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `passportdata`
--
ALTER TABLE `passportdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `assessment`
--
ALTER TABLE `assessment`
  ADD CONSTRAINT `assessment_ibfk_1` FOREIGN KEY (`mid`) REFERENCES `members` (`id`);

--
-- Constraints for table `educationaldata`
--
ALTER TABLE `educationaldata`
  ADD CONSTRAINT `educationaldata_ibfk_1` FOREIGN KEY (`mid`) REFERENCES `members` (`id`);

--
-- Constraints for table `experiencedata`
--
ALTER TABLE `experiencedata`
  ADD CONSTRAINT `experiencedata_ibfk_1` FOREIGN KEY (`mid`) REFERENCES `members` (`id`);

--
-- Constraints for table `passportdata`
--
ALTER TABLE `passportdata`
  ADD CONSTRAINT `passportdata_ibfk_1` FOREIGN KEY (`mid`) REFERENCES `members` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
